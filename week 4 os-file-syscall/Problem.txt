In this challange, you are supposed to implement following functions
int open_file_in_read_mode() - should open file in read mode and return the file descriptor
int open_file_in_read_write_mode( )- open file in read and write mode, return the file descriptor
size_t read_file(int fd,char *buffer,int length) - Read the data from the file, fd - File descriptor buffer - Buffer where the data needs to be read length - the length of the buffer. This function should return the amount of data that has been read.
size_t write_file(int fd,char *data) - Write the data to the file. fd - File Descriptor for the file where the data needs to be written. data - Contains the string that needs to be written.

Input Format

Data CREATE_FLAG Data - the data to be written/read CREATE_FLAG - will create the file or not depending on the flag value.

Constraints

0<1024

Output Format

File Open Successful File Open Successful File Open Successful Data Read:12 Buffer:12

Sample Input 0

12
0
Sample Output 0

File Open Failed
Sample Input 1

12
1
Sample Output 1

File Open Successful
File Open Successful
File Open Successful
Data Read:12
Buffer:12