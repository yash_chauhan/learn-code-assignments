You are given a pointer to the list containing head and tail of the linked list as a parameter. head and tail both are initialized to NULL.

You need to insert a node to the list. Both head and tail should point to this new node.

Input Format

You have to complete the "add" method which takes 2 arguments, The linked_list containing head and tail of the linked list and an integer that needs to be inserted in the list.

The input reading is handled by the code hence you need not worry about reading from stdin/stdout.

The first line contains an integer n which corresponds to the number of elements in the list. The following lines contains the elements of the list.

Constraints

1<=n>=100

Output Format

Add the new node at the head of linked list. No need to print anything on the stdout.

Sample Input 0

1
3
Sample Output 0

List : 3,