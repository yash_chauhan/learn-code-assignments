import threading 
balance = 100
lock = threading.Lock()

def deposit(): 
    global balance
    lock.acquire()
    for i in range(1000000):  
        balance = balance + 1
    lock.release()
        
def withdraw(): 
    global balance
    lock.acquire()
    for i in range(1000000):   
        balance = balance - 1
    lock.release()

if __name__ == "__main__": 
    balance = int(input().strip())
    thread1 = threading.Thread(target=deposit, args=(lock,)) 
    thread2 = threading.Thread(target=withdraw, args=(lock,))  
    thread1.start() 
    thread2.start()  
    thread1.join()  
    thread2.join() 

    print("all done: balance = " + str(balance))