import java.util.Scanner;

public class BackAssignmentTwo {

    static int totalAmount = 1000000;

    public static void main(String[] args) {
        Scanner sc= new Scanner(System.in);
        System.out.println("Enter the required amount");
        int amount = sc.nextInt();

        try {
            if (withDrawAmount(amount) == false) {
                throw new InsufficientFundException("The amount which you want to withdraw is over the balance amount");
            }
        } catch (InsufficientFundException insufficientFundException) {
            //can put condition to try again also
        }
        finally {
            System.out.println("Close the account");
        }
}

    public static Boolean withDrawAmount(int amount) {
    Boolean status = false;
        if (amount <= totalAmount) {
            System.out.println("Amount is withdrawn and the balance amount is");
            System.out.println((totalAmount - amount));
            status = true;
        } else {
         System.out.println("The amount is insufficient");
            status = false;
        }
        return status;
    }
}

class InsufficientFundException extends RuntimeException {
    InsufficientFundException(String statement) {
        super(statement);
    }
}